A chat app that demonstrates lit-apollo web components that use subscriptions and mutations.

# Installation
```
npm i
```

# Run Locally
```
npm start
```

# Development Watch
```
npm run watch
```
